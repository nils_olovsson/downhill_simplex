**Downhill Simplex**
A python implementation of the downhill simplex optimization method by Nelder
and Mead for the special case of a 2D optimization problem.

Running the script will solve an example problem, plot the first iterations and
save the figures as well as making an animation that illustrates how the simplex
_moves_ toward the minimum on the domain.

Further, a set of images that illustrate the five different transforms the
simplex undergo make are created.

Blog post:
http://www.all-systems-phenomenal.com/articles/downhill_simplex

**License: MIT**
