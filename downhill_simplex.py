#!/usr/bin/env python3

"""
author:  Nils Olofsson
email:   nils.olovsson@gmail.com
website: http://nilsolovsson.se
date:    2019-11-25
license: MIT

Example implementing the downhill simplex method to solve a smooth
minimization problem in 2D.
Further, plots demonstrating the iteration pattern of the simplex
method are created and saved.
"""

import os
import copy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.ticker as ticker

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
from matplotlib import animation

# ----------------------------------------------------------
# Set global background colors for all plots so it fits
# publishing backgound.
# Usually #ffffff or a softer color #f5f5f0.
#
# Colors of indivual plots can be set by:
#   fig.patch.set_facecolor(__COLOR__)
#   ax = fig.gca()
#   ax.set_facecolor(__COLOR__)
# ----------------------------------------------------------
bkg_color = '#ffffff'
bkg_color = '#ff00ff'
ax_color = bkg_color

plt.rcParams['figure.facecolor'] = bkg_color
plt.rcParams['axes.facecolor'] = ax_color

# ==============================================================================
#
# 2D optimizable hat like function used as example
#
# ==============================================================================

cm = plt.cm.coolwarm

#delta = 0.01
delta = 0.05
#x = y = np.arange(-4.0, 4.00, delta)
#X, Y = np.meshgrid(x, y)

s = 0.75
t = 1

def genGrid(delta):
    """
        Generate the X, Y grid for 2D and 3D plots
    """
    x = y = np.arange(-4.0, 4.00, delta)
    X, Y = np.meshgrid(x, y)
    return X, Y, x, y

def hat(p):
    """
        A DoG hat function that grows far from origo, evaluated at a point
    """
    x = p[0]
    y = p[1]
    z1 = np.exp( -(x*x+y*y)/(2*t*t) )
    z2 = np.exp( -(x*x+y*y)/(2*s*s) )
    z3 = (0.10*x)**2 + (0.10*y)**2
    return z1 - z2 + z3

def hatGrid(X, Y):
    """
        A DoG hat function that grows far from origo, evaluated on a grid
    """
    Z1 = np.exp(-(X**2+Y**2)/(2*t*t))
    Z2 = np.exp(-(X**2+Y**2)/(2*s*s))
    Z3 = (0.10*X)**2 + (0.10*Y)**2
    Z = Z1 - Z2 + Z3 
    return Z

# ==============================================================================
#
# Plot the example function in 2D and 3D
#
# ==============================================================================

def plot2D(simplex_matrix, Z, extent, iteration=0):
    """
    """

    my_dpi=96
    fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    ax = fig.gca()
    
    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)
    
    CS = ax.imshow(Z, origin='lower', cmap=cm, extent=extent)

    #simplex.sort()
    N = 3
    for i in range(0, N):
        j = i+1;
        if(i==N-1):
            j = 0
        #p0 = simplex[i].p
        #p1 = simplex[j].p

        p0 = simplex_matrix[i,:]
        p1 = simplex_matrix[j,:]

        l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
        l.set_color('black')
        l.set_linewidth(2)
        ax.add_line(l)

    x_simp = np.zeros(N)
    y_simp = np.zeros(N)

    for i in range(0, N):
        x_simp[i] = simplex_matrix[i,0]
        y_simp[i] = simplex_matrix[i,1]

    colors = ['blue','green','red']
    ax.scatter(x_simp, y_simp, s=80, zorder=3, edgecolors='black', color=colors)

    plt.text(extent[0]+0.1, extent[2]+0.1, chr(65+iteration), fontsize=64)

    ax.axis('off')

    #plt.subplots_adjust(top = 4, bottom = -4, right = 4, left = -4,  hspace = 0, wspace = 0)
    plt.subplots_adjust(hspace = 0, wspace = 0)

    plt.gca().margins(0,0)
    plt.gca().xaxis.set_major_locator(ticker.NullLocator())
    plt.gca().yaxis.set_major_locator(ticker.NullLocator())

    plt.savefig('iterations/nr_{}.png'.format(iteration), bbox_inches='tight',
                facecolor=fig.get_facecolor(), edgecolor='none')
    plt.close()

# ----------------------------------------------------------
# Create 3D surface (edges) plot
# ----------------------------------------------------------
def plot3D():
    """
    """

    delta = 0.05
    X, Y, _, _ = genGrid(delta)
    Z = hatGrid(X, Y)

    norm = plt.Normalize(Z.min(), Z.max())
    colors = cm(norm(Z))
    rcount, ccount, _ = colors.shape

    fig = plt.figure(figsize=(20, 10))
    ax   = fig.gca(projection='3d')
    
    surf = ax.plot_surface(X, Y, Z, cmap=cm, rstride=3, cstride=3,
            facecolors=colors, alpha=0.75, shade=False, linewidths=1, zorder=4)
    surf.set_edgecolor([0.25, 0.25, 0.25, 1])
    ax._axis3don = False

    cset = ax.contour(X, Y, Z, zdir='x', offset=-4, cmap=cm, levels=10)
    cset = ax.contour(X, Y, Z, zdir='y', offset=4, cmap=cm, levels=10)

    plt.savefig('hatsimp.svg', bbox_inches='tight', pad_inches=0, dpi=96)

    #plt.show(block=True)
    plt.close()

# ==============================================================================
#
# Simplex optimizer class 
#
# ==============================================================================

class Vertex:
    """
        A vertex for this problem is a point in 2D space, x,  with an associated
        function value f = f(x).
        A collection of vertices can be sorted according to f.
    """
    def __init__(self):
        self.p = np.array([5,5], dtype=np.float64)
        self.v = np.finfo(np.float64).max

    def __lt__(self, other):
        return self.v < other.v

class SimplexOptimizer:
    """
    """
    def __init__(self, function):
        
        # The objective function, triangle simplex and edge centroid
        self.f = function
        self.simplex = []
        self.simplex.append(Vertex())
        self.simplex.append(Vertex())
        self.simplex.append(Vertex())
        
        self.c = np.array([0,0], dtype=np.float64)

        # The history is kept as a 3D array
        self.history_size = 20
        self.nr_to_plot   = 6 
        self.history      = np.zeros([self.history_size, 6, 2],
                                     dtype=np.float64)

        # Number of total iterations to run, I don't use
        # any termination criteria) and statistics
        self.max_iter  = 100
        self.iteration = 0

        self.reflections          = 0
        self.expansions           = 0
        self.contractions_outside = 0
        self.contractions_inside  = 0
        self.shrinks              = 0

    def printf(self):
        msg = ''
        msg += 'Final simplex and function values\n'
        for v in self.simplex:
            msg += '{} - {}\n'.format(v.p, v.v)

        msg += 'Nr of reflections:      {}\n'.format(self.reflections)
        msg += 'Nr of expansions:       {}\n'.format(self.expansions)
        msg += 'Nr of contractions out: {}\n'.format(self.contractions_outside)
        msg += 'Nr of contractions in:  {}\n'.format(self.contractions_inside)
        msg += 'Nr of shrinks:          {}\n'.format(self.shrinks)
        
        print('Results:\n{}\n'.format(msg))

        file = open('result.txt','w') 
        file.write(msg) 
        file.close() 

    def sort(self):
        self.simplex.sort()

    def centroid(self):
        return 0.5*(self.simplex[0].p + self.simplex[1].p)

    def reflection(self):
        alpha = 1
        c = self.centroid()
        x = c + alpha*(c-self.simplex[2].p)
        return x

    def expansion(self, xr):
        gamma = 2
        c = self.centroid()
        x = c + gamma*(xr - c)
        return x

    def contraction(self, pos):
        beta = 0.5
        c = self.centroid()
        x = c + beta*(pos - c)
        return x
    
    def shrinkContract(self):
        delta = 0.5
        self.simplex[1].p = self.simplex[0].p + delta*(self.simplex[1].p-self.simplex[0].p)
        self.simplex[2].p = self.simplex[0].p + delta*(self.simplex[2].p-self.simplex[0].p)

    def eval(self):
        for vertex in self.simplex:
            vertex.v = self.f(vertex.p)

    def solve(self):
        self.eval()
        self.sort()
        self.iteration = 0
        self.history = np.zeros([self.history_size, 6, 2], dtype=np.float64)
        
        for i in range(0, self.max_iter):

            self.iteration += 1

            if self.history_size > i:
                self.history[i,0,:] = self.simplex[0].p
                self.history[i,1,:] = self.simplex[1].p
                self.history[i,2,:] = self.simplex[2].p

            xr = self.reflection()
            vr = self.f(xr)

            shrink = False

            if vr<self.simplex[1].v and vr>=self.simplex[0].v:
                self.simplex[2].p = xr
                self.simplex[2].v = vr
                self.reflections += 1
            elif vr<self.simplex[0].v:
                xe = self.expansion(xr)
                ve = self.f(xe)
                # Use greedy expansion
                if ve < self.simplex[0].v and vr < self.simplex[0].v:
                    self.simplex[2].p = xe
                    self.simplex[2].v = ve
                else:
                    self.simplex[2].p = xr
                    self.simplex[2].v = vr
                self.expansions += 1
            elif vr>=self.simplex[1].v:
                if vr < self.simplex[0].v:
                    # Contraction Outside
                    xc = self.contraction(xr)
                    vc = self.f(xc)
                    if vc <= vr:
                        self.simplex[2].p = xc
                        self.simplex[2].v = vc
                        self.contractions_outside += 1
                    else:
                        shrink = True
                else: # v >= self.simplex[0].v
                    # Contraction inside
                    xc = self.contraction(self.simplex[2].p)
                    vc = self.f(xc)
                    if vc <= self.simplex[2].v:
                        self.simplex[2].p = xc
                        self.simplex[2].v = vc
                        self.contractions_inside += 1
                    else:
                        shrink = True
            else:
                shrink = True

            if shrink:
                self.shrinkContract()
                self.simplex[1].v = self.f(self.simplex[1].p)
                self.simplex[2].v = self.f(self.simplex[2].p)
                self.shrinks += 1

            # Append transformed vertex
            if self.history_size > i:
                self.history[i,3,:] = self.simplex[0].p
                self.history[i,4,:] = self.simplex[1].p
                self.history[i,5,:] = self.simplex[2].p

            self.sort()
        return self.simplex[0].p

    def plotResults(self, Z, extent):
        for i in range(self.nr_to_plot):
            simplex_matrix = self.history[i,0:4,:]
            plot2D(simplex_matrix, Z, extent, i)

# ==============================================================================
#
# Plot animated amoeba
#
# ==============================================================================

def animateAmoeba(simplices, Z, extent):

    my_dpi=96
    fig = plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    ax = fig.gca()
    
    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)
    
    img   = []
    lines = []
    artists = []

    N, M, P = simplices.shape

    seconds_per_step = 2
    samples_per_second = 40
    samples_per_step = seconds_per_step*samples_per_second
    frames = N*samples_per_step
    fps    = samples_per_step / seconds_per_step

    def smoothstep(t, t0, t1):
        t = (t-t0)/(t1-t0)
        return t*t*(3-2*t)

    def initAnimation():
        #global fig, ax
       
        # Initialize the background 2D function plot
        img = ax.imshow(Z, origin='lower',cmap=cm, extent=extent)
        artists.append(img)
       
        # Initalize the text field
        text = plt.text(x[0]+0.01, y[0]+0.1, '', fontsize=28)
        artists.append(text)
        simplex = simplices[0,:,:]
        
        # Initialize the vertices
        x_simp = np.zeros(3)
        y_simp = np.zeros(3)
        for i in range(3):
            x_simp[i] = simplex[i,0]
            y_simp[i] = simplex[i,1]
        colors = ['blue', 'green', 'red']
        scatter = ax.scatter(x_simp, y_simp, s=80, color=colors, zorder=3, edgecolors='black')
        artists.append(scatter)

        # Initialize the lines
        for ind0 in range(3):
            ind1 = ind0+1;
            if(ind0==2):
                ind1 = 0
            p0 = simplex[ind0,:]
            p1 = simplex[ind1,:]

            l = mlines.Line2D([p0[0],p1[0]], [p0[1],p1[1]])
            l.set_color('black')
            l.set_linewidth(2)
            ax.add_line(l)
        
            artists.append(l)
        ax.axis('off')
        
        plt.subplots_adjust(hspace = None, wspace = None)
        
        plt.gca().margins(0,0)
        plt.gca().xaxis.set_major_locator(ticker.NullLocator())
        plt.gca().yaxis.set_major_locator(ticker.NullLocator())
        
        return artists

    def animate(i):

        simplex_ind = int(np.floor(i / samples_per_step))
        simplex = simplices[simplex_ind,:,:].copy()

        step = i % samples_per_step
        
        text = artists[1]
        text.set_text('Iteration: {}/{}'.format(simplex_ind+1, len(simplices)))

        # Smooth interpolation between the
        # old vertex positions and the new ones
        t = smoothstep(step, 0, samples_per_step)
        simplex[0,:] = t*simplex[3,:] + (1-t)*simplex[0,:]
        simplex[1,:] = t*simplex[4,:] + (1-t)*simplex[1,:]
        simplex[2,:] = t*simplex[5,:] + (1-t)*simplex[2,:]

        #if step==0:
        #    print('Animate: {}/{}'.format(simplex_ind+1, len(simplices)))

        # Update the verteices
        vertices = simplex[0:3,:]
        scatter = artists[2]
        scatter.set_offsets(vertices)

        # Update the lines
        for ind0 in range(3):
            ind1 = ind0+1;
            if(ind1==3):
                ind1 = 0
            p0 = simplex[ind0,:]
            p1 = simplex[ind1,:]

            line_index = ind0+3
            line = artists[line_index]
            line.set_xdata([p0[0], p1[0]])
            line.set_ydata([p0[1], p1[1]])

        return artists

    anim = animation.FuncAnimation(fig, animate, init_func=initAnimation,
                                   frames=frames, blit=True)

    kwargs = {'pad_inches': 0, 'facecolor': bkg_color, 'edgecolor': 'none'}
    anim.save('animation/amoeba_animation.mp4', fps=fps, extra_args=['-vcodec', 'libx264'],
            dpi=96, savefig_kwargs=kwargs)

    plt.close()


# ==============================================================================
#
# Plot examples of the simplex transforms in 2D
#
# ==============================================================================

# ------------------------------------------------
# Bold font latex labels
# ------------------------------------------------
lbl_x0 = "$\\textbf{x}_0$"
lbl_x1 = "$\\textbf{x}_1$"
lbl_x2 = "$\\textbf{x}_2$"
lbl_xr = "$\\textbf{x}_r$"
lbl_xe = "$\\textbf{x}_e$"
lbl_xc = "$\\textbf{x}_c$"
lbl_s0 = "$\\textbf{s}_0$"
lbl_s1 = "$\\textbf{s}_1$"
lbl_c  = "$\\textbf{c}$"

def plotColoredLines(ax, lines, colors):
    """
    """

    for i, l in enumerate(lines):
            l.set_color(colors[i])
            l.set_linewidth(2)
            ax.add_line(l)

def plotAnnotatedPoints(ax, points, colors, labels):
    """
    """
    
    xs, ys = np.zeros(len(points)), np.zeros(len(points))
    for i, point in enumerate(points):
        xs[i] = point[0]
        ys[i] = point[1]

    ax.scatter(xs, ys, s=80, zorder=3, edgecolors='black', color=colors)

    for i, point in enumerate(points):
        label = labels[i]
        offset = (0,10)
        if point[1]<0.5:
            offset = (0,-25)
        plt.annotate(label, point, fontsize=28, textcoords="offset points", xytext=offset, ha='center')


def plotReflection(simplex):
    """

    """
    
    # Initialize plot
    rc('font',**{'family':'DejaVu Sans','sans-serif':['DejaVu']})
    rc('text', usetex=True)

    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()
    ax.axis('off')

    # Plot colored lines
    lines = []
    lines.append(mlines.Line2D([simplex['0'][0],simplex['1'][0]], [simplex['0'][1],simplex['1'][1]]))
    lines.append(mlines.Line2D([simplex['0'][0],simplex['r'][0]], [simplex['0'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],simplex['r'][0]], [simplex['1'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['0'][0]], [simplex['2'][1],simplex['0'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['1'][0]], [simplex['2'][1],simplex['1'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['r'][0]], [simplex['2'][1],simplex['r'][1]]))

    lines[-1].set_dashes([4,2])

    colors = ['red', 'red', 'red', 'blue', 'blue', 'black']
    plotColoredLines(ax, lines, colors)

    # Plot labeled and colored points
    points = [simplex['0'], simplex['1'], simplex['2'], simplex['r'], simplex['c']]
    colors = ['blue','green','red', 'yellow', 'black']
    labels = [lbl_x0, lbl_x1, lbl_x2, lbl_xr, lbl_c]
    plotAnnotatedPoints(ax, points, colors, labels)
    
    # Finalize plot
    plt.savefig('transforms/reflect.svg', bbox_inches='tight', pad_inches=0,
                facecolor=fig.get_facecolor(), edgecolor='none')
    plt.close()
    rc('text', usetex=False)
    
def plotExpand(simplex):
    """

    """
    
    # Initialize plot
    rc('font',**{'family':'DejaVu Sans','sans-serif':['DejaVu']})
    rc('text', usetex=True)

    fig = plt.figure(figsize=(12, 7.5))
    ax = fig.gca()
    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)
    ax.axis('off')

    # Plot colored lines
    lines = []
    lines.append(mlines.Line2D([simplex['0'][0],simplex['1'][0]], [simplex['0'][1],simplex['1'][1]]))
    lines.append(mlines.Line2D([simplex['0'][0],simplex['e'][0]], [simplex['0'][1],simplex['e'][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],simplex['e'][0]], [simplex['1'][1],simplex['e'][1]]))
    
    lines.append(mlines.Line2D([simplex['2'][0],simplex['0'][0]], [simplex['2'][1],simplex['0'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['1'][0]], [simplex['2'][1],simplex['1'][1]]))
    
    lines.append(mlines.Line2D([simplex['0'][0],simplex['r'][0]], [simplex['0'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],simplex['r'][0]], [simplex['1'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['e'][0]], [simplex['2'][1],simplex['e'][1]]))
    
    lines[-1].set_dashes([4,2])
    lines[-2].set_dashes([4,2])
    lines[-3].set_dashes([4,2])

    colors = ['red', 'red', 'red', 'blue', 'blue', 'black', 'black', 'black']
    plotColoredLines(ax, lines, colors)

    # Plot labeled and colored points
    points = [simplex['0'], simplex['1'], simplex['2'], simplex['r'], simplex['c'], simplex['e']]
    colors = ['blue','green','red', 'black', 'black', 'yellow']
    labels = [lbl_x0, lbl_x1, lbl_x2, lbl_xr, lbl_c, lbl_xe]
    plotAnnotatedPoints(ax, points, colors, labels)

    # Finalize plot
    plt.savefig('transforms/expand.svg', bbox_inches='tight', pad_inches=0,
                facecolor=fig.get_facecolor(), edgecolor='none')
    plt.close()
    rc('text', usetex=False)
    
def plotContract(simplex, cnt='o'):
    """

    """
    
    # Initialize plot
    rc('font',**{'family':'DejaVu Sans','sans-serif':['DejaVu']})
    rc('text', usetex=True)

    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()
    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)
    ax.axis('off')

    # Plot colored lines
    lines = []
    lines.append(mlines.Line2D([simplex['0'][0],simplex['1'][0]], [simplex['0'][1],simplex['1'][1]]))
    lines.append(mlines.Line2D([simplex['0'][0],simplex[cnt][0]], [simplex['0'][1],simplex[cnt][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],simplex[cnt][0]], [simplex['1'][1],simplex[cnt][1]]))
    
    lines.append(mlines.Line2D([simplex['2'][0],simplex['0'][0]], [simplex['2'][1],simplex['0'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['1'][0]], [simplex['2'][1],simplex['1'][1]]))
    
    lines.append(mlines.Line2D([simplex['0'][0],simplex['r'][0]], [simplex['0'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],simplex['r'][0]], [simplex['1'][1],simplex['r'][1]]))
    lines.append(mlines.Line2D([simplex['2'][0],simplex['r'][0]], [simplex['2'][1],simplex['r'][1]]))

    lines[-1].set_dashes([4,2])
    lines[-2].set_dashes([4,2])
    lines[-3].set_dashes([4,2])

    colors = ['red', 'red', 'red', 'blue', 'blue', 'black', 'black', 'black']
    plotColoredLines(ax, lines, colors)

    # Plot labeled and colored points
    points = [simplex['0'], simplex['1'], simplex['2'], simplex['r'], simplex['c'], simplex[cnt]]
    colors = ['blue','green','red', 'black', 'black', 'yellow']
    labels = [lbl_x0, lbl_x1, lbl_x2, lbl_xr, lbl_c, lbl_xc]
    plotAnnotatedPoints(ax, points, colors, labels)

    # Finalize plot
    plt.savefig('transforms/contract_{}.svg'.format(cnt), bbox_inches='tight', pad_inches=0,
                facecolor=fig.get_facecolor(), edgecolor='none')
    plt.close()
    rc('text', usetex=False)

def plotShrink(simplex):
    """

    """
    
    # Initialize plot
    rc('font',**{'family':'DejaVu Sans','sans-serif':['DejaVu']})
    rc('text', usetex=True)

    fig = plt.figure(figsize=(8, 5))
    ax = fig.gca()
    fig.patch.set_facecolor(bkg_color)
    ax.set_facecolor(ax_color)
    ax.axis('off')

    # Compute the shrink points
    delta = 0.5
    s0 = simplex['0'] + delta*(simplex['1'] - simplex['0'])
    s1 = simplex['0'] + delta*(simplex['2'] - simplex['0'])

    # Plot colored lines
    lines = []
    lines.append(mlines.Line2D([simplex['0'][0],s0[0]], [simplex['0'][1],s0[1]]))
    lines.append(mlines.Line2D([simplex['0'][0],s1[0]], [simplex['0'][1],s1[1]]))
    lines.append(mlines.Line2D([s0[0],s1[0]], [s0[1],s1[1]]))

    lines.append(mlines.Line2D([simplex['1'][0],simplex['2'][0]], [simplex['1'][1],simplex['2'][1]]))
    lines.append(mlines.Line2D([simplex['1'][0],s0[0]], [simplex['1'][1],s0[1]]))
    lines.append(mlines.Line2D([simplex['2'][0],s1[0]], [simplex['2'][1],s1[1]]))

    colors = ['red', 'red', 'red', 'blue', 'blue', 'blue']
    plotColoredLines(ax, lines, colors)

    # Plot labeled and colored points
    points = [simplex['0'], simplex['1'], simplex['2'], s0, s1]
    colors = ['blue','green','red', 'yellow', 'yellow']
    labels = ["$x_0$", "$x_1$", "$x_2$", "$s_0$", "$s_1$"]
    labels = [lbl_x0, lbl_x1, lbl_x2, lbl_s0, lbl_s1]
    plotAnnotatedPoints(ax, points, colors, labels)

    # Finalize plot
    plt.savefig('transforms/shrink.svg', bbox_inches='tight', pad_inches=0,
                facecolor=fig.get_facecolor(), edgecolor='none')
    plt.close()
    rc('text', usetex=False)

# ==============================================================================
#
# Main 
#
# ==============================================================================

if __name__ == "__main__":
    
    # ------------------------------------------------------
    # Run the downhill simplex and plot first iterations
    # ------------------------------------------------------
    x_simp = np.array([0.25, 3.0,  2.5], dtype=np.float64)
    y_simp = np.array([0.25, 3.0, -2.0], dtype=np.float64)

    so = SimplexOptimizer(function=hat)
    so.simplex[0].p = np.array([x_simp[0], y_simp[0]], dtype=np.float64)
    so.simplex[1].p = np.array([x_simp[1], y_simp[1]], dtype=np.float64)
    so.simplex[2].p = np.array([x_simp[2], y_simp[2]], dtype=np.float64)

    so.eval()
    so.sort()
    p = so.solve()
    so.printf()

    print('Solved: {} - {}'.format(p, hat(p)))
    print('Iterations: {} of {}'.format(so.iteration, so.max_iter))

    X, Y, x, y = genGrid(0.01)
    Z = hatGrid(X, Y)
    extent = [x[0], x[-1], y[0], y[-1]]

    so.plotResults(Z, extent)
    animateAmoeba(so.history, Z, extent)

    # ------------------------------------------------------
    # Create figures for an example of the different
    # kinds of simplex transforms
    # ------------------------------------------------------

    simplex = {}
    simplex['0'] = np.array([1.00, 0.00], dtype=np.float64)
    simplex['1'] = np.array([0.25, 1.00], dtype=np.float64)
    simplex['2'] = np.array([0.00, 0.00], dtype=np.float64)

    simplex['c'] = 0.5*(simplex['0'] + simplex['1'])              # Centroid
    simplex['r'] = simplex['c'] + 1.0*(simplex['c']-simplex['2']) # Reflection
    simplex['e'] = simplex['c'] + simplex['r']                    # Expansion
    simplex['o'] = simplex['c'] + 0.5*(simplex['r']-simplex['c']) # Contraction outside
    simplex['i'] = simplex['c'] + 0.5*(simplex['2']-simplex['c']) # Contraction inside
   
    plotReflection(simplex)
    plotExpand(simplex)
    plotContract(simplex, 'o')
    plotContract(simplex, 'i')
    plotShrink(simplex)

    plot3D()
