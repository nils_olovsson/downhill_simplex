#!/bin/bash
# This will crop the images made by the python script so that borders are removed

echo "Cropping images..."
for img in *.png; do
	#name=${img:3:-4}
    #echo $name
    echo "    $img"
    convert $img -crop 615x615+10+9 $img
done
echo "...done"
