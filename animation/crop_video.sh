#!/bin/bash
# This will crop the video made by the python script so that borders are removed

ffmpeg -i amoeba_animation.mp4 -filter:v "crop=615:615:102:96" .cropped_animation.mp4
rm amoeba_animation.mp4
mv .cropped_animation.mp4 amoeba_animation.mp4
